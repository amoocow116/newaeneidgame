// Name: Camera.as
// Author(s): Jason Wang
// version 0.1
// --------------------------------------
// This class defines the Camera, which
// is used to allow the players to see
// Aeneas and the map. This camera will
// be rubber, which means it gradually
// speeds up and slows down while
// following Aeneas.

// Notes:
// To construct, use default constructor
// void setSensor( int x, int y )
/*
		ee::Camera cameraEntity;
		cameraEntity.setSensor( float x, float y );
		cameraEntity.rotate( int degrees );
		cameraEntity.setRotation( int degrees );
		cameraEntity.setSize( float x, float y );
		ee::getWindowHeight();
		ee::getWindowWidth();
		cameraEntity.setViewPort( float x1, float y1, float x2, float y2 );
		*/

#include "include/Movable.as"
import CharPosition @ getAeneasPos() from "Aeneas.as";
import float getAeneasWalkSpeed() from "Aeneas.as";
import CharStats @ getAeneasStats() from "Aeneas.as";
import bool isReadyToChangeHealth() from "Aeneas.as";

// -------------------------------------------------------------------------------

Camera camera;
HUD hud;

void initialize() {
	ee::consolePrintln( "initialize in camera.as");
	camera = Camera();
	do { // Makes sure Camera has an actual Aeneas position to focus on
		bool b = camera.setupPosition(); // Once true, this DOES IN FACT automatically set up position
	} while ( !b );

	hud = HUD();
}

void step( uint32 milliseconds ) {
	// To calculate speed of camera, use x^1.5 per distance, with cap of Aeneas' walkSpeed
	ee::consolePrintln( "step function in camera.as");
	camera.update( milliseconds );

	bool b;
	do { // Keeps looping while health bar is not ready to be changed
		b = isReadyToChangeHealth();
	} while( !b );
	ee::consolePrintln( "gets out of the do while loop in step function of camera.as");
	camera.updateHUD();
}

Camera @ getCamera() { return camera; }

// ------------------------------- CAMERA CLASS BELOW ------------------------------------------ //

shared class Camera : Movable {

	// NOTE: Point of origin is the bottom left for the camera
	private CharPosition pos; // Camera position
	private CharPosition @ aeneasPos; // Aeneas' position; reference, so doesn't need to be updated
	float walkSpeed;
	ee::Camera cameraEntity;
	HUD headsUp;

	Camera() {
		ee::consolePrintln( "camera constructor");
		Movable();
		pos = CharPosition();
		headsUp = HUD();
		if( getAeneasPos() != null ) {
			aeneasPos = getAeneasPos();
		} else {
			ee::consolePrintLine( "ERROR: Camera.as cannot retrieve Aeneas position." );
			aeneasPos = CharPosition(0, 0, 0);
		}

		cameraEntity.setSize( ee::getWindowWidth(), ee::getWindowHeight() );
	}

	Camera( int x, int y, double angle ) {
		ee::consolePrintln( "overloaded camera constructor");
		Movable( x, y, angle );
		pos = CharPosition();
		headsUp=HUD();
		if( getAeneasPos() != null ) {
			aeneasPos = getAeneasPos();
		} else {
			ee::consolePrintLine( "ERROR: Camera.as cannot retrieve Aeneas position." );
			aeneasPos = CharPosition(0, 0, 0); 
		}

		cameraEntity.setSize( ee::getWindowWidth(), ee::getWindowHeight() );
	}

	// Called when Aeneas is dead
	void resetPos() {
		ee::consolePrintln( "resetpos() in camera.as");
		pos.setX( aeneasPos.getX() );
		pos.setY( aeneasPos.getY() );
	}

	bool setupPosition() {
		ee::consolePrintln( "setupposition() in camera.as");
		if( getAeneasPos() != null ) {
			aeneasPos = getAeneasPos();

			// WARNING: DO NOT SET POS = AENEASPOS;
			updatePos( aeneasPos.x, aeneasPos.y, aeneasPos.angle );
			walkSpeed = getAeneasWalkSpeed();
			ee::consolePrintLine( "Camera.as/ Walkspeed has been set along with position." );
			return true;
		}
		return false;
	}

	void updatePos( int x, int y ) {
		ee::consolePrintln( "updatepos in camera.as")
		pos.setPos( x, y, 0 );
		cameraEntity.setCenter( x, y, 0 );
		headsUp.setPosition( x, y );
	}

	void update( uint32 milliseconds ) {
		ee::consolePrintln( "update(uint32) in camera.as");
		// To calculate speed of camera, use x^1.5 per distance, with cap of Aeneas' walkSpeed
		float xDif = float(pos.x) - aeneasPos.x; //incorrect typecasting?
		float yDif = float(pos.y) - aeneasPos.y;

		float distance = sqrt( xDif*xDif + yDif*yDif );

		// Arbitrary camera speed (rubber based), complex formula I basically randomly picked
		// For math: could use variation on population logistic growth formula
		float cameraSpeed = walkSpeed * pow( distance * 1.25 / walkSpeed, 1.5 );
		if( cameraSpeed > walkSpeed )
			cameraSpeed = walkSpeed;

		float cameraDistance = cameraSpeed * milliseconds / 1000;

		// Only works if the "origin" of the camera is at bottom left of the screen
		updatePos( int(xDif + pos.x), int(yDif + pos.y) ); // NOTE: Check to see if this works

	}

	void updateHUD() {
		ee::consolePrintln( "updateHUD in camera.as");
		// Changes health bar
		float curHealth = getAeneasStats().getCHealth();
		float maxHealth = getAeneasStats().getMHealth();

		headsUp.changeHealth( float(curHealth) / maxHealth ); // Check if first param is float or int
	}
}

// TODO: Add more content
shared class HUD 
{
	// Point of origin (POO)
	ee::StaticEntity health("HUD", "healthBar"); // POO - top left
	ee::StaticEntity healthFrame("HUD", "healthBarFrame"); // POO - top left
	ee::StaticEntity objective("HUD","objective"); // POO - top right
	ee::StaticEntity objective2("HUD","objective2"); // POO - top right
	ee::StaticEntity objective3("HUD","objective3"); // POO - top right
	ee::StaticEntity objective4("HUD","objective4"); // POO - top right
	
	HUD()
	{
		ee::consolePrintln( "HUD constructor");
		health.setPosition( pos.x - ee::getWindowWidth() / 2, pos.y - ee::getWindowHeight() * 3 / 8 );
		healthFrame.setPosition( pos.x - ee::getWindowWidth() / 2, pos.y + ee::getWindowHeight() / 2 );
		objective.setPosition( pos.x, pos.y + ee::getWindowHeight() / 2 );
		objective2.setPosition( pos.x, pos.y + ee::getWindowHeight() / 2 );
		objective3.setPosition( pos.x, pos.y + ee::getWindowHeight() / 2 );
		objective4.setPosition( pos.x, pos.y + ee::getWindowHeight() / 2 );
		objective2.setVisible(false);
		objective3.setVisible(false);
		objective4.setVisible(false);
	}
	
	void changeObjective2()
	{
		ee::consolePrintln( "hud changeobj2");
		objective.setVisible(false);
		objective2.setVisible(true);
	}
	
	void changeObjective3
	{
		ee::consolePrintln( "hud changeobj3");
		objective2.setVisible(false);
		objective3.setVisible(true);
	}
	
	void changeObjective4()
	{
		ee::consolePrintln( "hud changeobj4");
		objective3.setVisible(false);
		objective4.setVisible(true);
	}
	void changeHealth( float percentage )
	{
		ee::consolePrintln( "hud changehealth");
		health.setScale( percentage, getScaleY() );//something about changing the image/sprite of the health bar DENNIS
		//this doesn't change the health statistic, this has to change the animation of the HUD to show visible change
	}
	
	ee::StaticEntity @ getHealth() //error
	{
		return health;
	}
	
	ee::StaticEntity @ getObjective() //error
	{
		return objective;
	}
	
	void moveXHUD(int x)
	{
		health.move(x,0);
		objective.move(x,0);
		objective2.move(x,0);
		objective3.move(x,0);
		objective4.move(x,0);
	}
	
	void moveYHUD(int y)
	{
	
		health.move(0,y);
		objective.move(x,0);
		objective2.move(x,0);
		objective3.move(x,0);
		objective4.move(x,0);
	}
	
	void moveXYHUD(int x, int y)
	{
		health.move(x,y);
		objective.move(x,0);
		objective2.move(x,0);
		objective3.move(x,0);
		objective4.move(x,0);
	}
	
	void setPosition(int x, int y)
	{
		health.setPosition(float(x),float(y));
		objective.setPosition(float(x),float(y));
		objective2.setPosition(float(x),float(y));
		objective3.setPosition(float(x),float(y));
		objective4.setPosition(float(x),float(y));
	}
}

