#include "include/World.as"
import void setUpAeneasSpawn( string name ) from "Aeneas.as"
import void setUpTrojansAndGreeks( string name ) from "TrojanGreek.as"

World world;
int level = 1;
int exitX;
int exitY;

void initialize() {
	ee::consolePrintln( "initalize in worldloader");
	setUpWorld( "VillageTown", "villageTownMap" );
	exitX = 0;
	exitY = 0;
}

void setUpWorld( string contName, string entName ) {
	ee::consolePrintln( "setUpWorld in worldloader.as");
	world = World( contName );

	world.add( world.getStaticEntity( contName, entName ), entName );
	world.setName( contName );

	if( contName == "VillageTown" )
		setUpVillageTown();
	else if( contName == "PriamAltar" )
		setUpPriamAltar();
	else if( contName == "cityThree" )
		setUpCityThree();
	else if( contName == "cityTwo" )
		setUpCityTwo();
	else if( contName == "cityOne" )
		setUpCityOne();
}

void step( uint32 milliseconds ) {
	ee::consolePrintln( "worldloader step function");
	if( world.isCompleted() ) {
		destroyOldWorld( level );
		level++;
		determineNextWorld( level );
		world.saveData();
	}
	world.step( milliseconds );
}

void setUpCheckPoint() {
	ee::consolePrintln( "setupcheckpoint in worldloader.as");
	if( world.getName() == "VillageTown" )
		world.setUpCheckPoint( 2051, 1731 );
	else if( world.getName() == "cityThree" )
		world.setUpCheckPoint( 436, 1046 );
	else if( world.getName() == "cityTwo" )
		world.setUpCheckPoint( 40, 1831 );
	else if( world.getName() == "cityOne" )
		world.setUpCheckPoint( 23, 1830 );
	else if( world.getName() == "PriamAltar" )
		world.setUpCheckPoint( 1689, 1623 );
}

void setUpExitPoint() {
	ee::consolePrintln( "setupexitpoint in worldloader.as");
	if( world.getName() == "VillageTown" )
		world.setUpExitPoint( 2497, 3106 );
	else if( world.getName() == "cityThree" )
		world.setUpExitPoint( 1477, 815 );
	else if( world.getName() == "cityTwo" )
		world.setUpExitPoint( 794, 286 );
	else if( world.getName() == "cityOne" )
		world.setUpExitPoint( 1132, 1418 );
	else if( world.getName() == "PriamAltar" )
		world.setUpExitPoint( 1695, 360 );
}

// Determines which world will be loaded next
void determineNextWorld( int i ) {
	ee::consolePrintln( "determineNextWorld in worldloader.as");
	string name = "";
	if( i == 1 ) {
		setUpWorld( "VillageTown", "villageTownMap" );
		name = "VillageTown";
	} else if( i == 2 ) {
		setUpWorld( "cityThree", "cityThreeMap" );
		name = "cityThree";
	} else if( i == 3 ) {
		setUpWorld( "cityTwo", "cityTwoMap" );
		name = "cityTwo";
	} else if( i == 4 ) {
		setUpWorld( "cityOne", "cityOneMap" );
		name = "cityOne";
	} else if( i == 5 ) {
		setUpWorld( "PriamAltar", "priamAltarMap" );
		name = "PriamAltar";
	}
	ee::writeToDataCont( "World", "level", i );
}

void finishWorldSetup( string name ) {
	ee::consolePrintln( "finishWorldSetup in worldloader.as");
	if( name == "" )
		ee::consolePrintLine( "ERROR: WorldLoader.as/ Cannot find world name." );
	else
		setUpAeneasSpawn( name );

	setUpTrojansAndGreeks( name ) {
	setUpCheckPoint();
	setUpExitPoint();
}

// Put in art assets LOL
void setUpVillageTown() {
	ee::consolePrintln( "setUpVillageTown in worldloader.as");
	world.add( world.getStaticEntity( "VillageTown", "brickRoof"), "brickRoof" );
	world.setobj[world.setobj.length()-1].setPosition(1420,1262);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "clayPot" ), "clayPot" );
	world.setobj[world.setobj.length()-1].setPosition(922,2838);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "eastCrossThatchRoof" ), "eastCrossThatchRoof" );
	world.setobj[world.setobj.length()-1].setPosition(2422,1406);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "fourHouseNE" ), "fourHouseNE" );
	world.setobj[world.setobj.length()-1].setPosition(1110,76);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "fourHouseNW" ), "fourHouseNW" );
	world.setobj[world.setobj.length()-1].setPosition(790,132);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "fourHouseSE" ), "fourHouseSE" );
	world.setobj[world.setobj.length()-1].setPosition(1122,386);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "fourHouseSW" ), "fourHouseSW" );
	world.setobj[world.setobj.length()-1].setPosition(812,486);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "northEastGrayPlankHouse" ), "northEastGrayPlankHouse" );
	world.setobj[world.setobj.length()-1].setPosition(1934,256);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "northEastTree" ), "northEastTree" );
	world.setobj[world.setobj.length()-1].setPosition(1930,728);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "northEastTreeBottom" ), "northEastTreeBottom" );
	world.setobj[world.setobj.length()-1].setPosition(1956,948);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "northEastWall" ), "northEastWall" );
	world.setobj[world.setobj.length()-1].setPosition(1874,480);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "northEastWoodRoofHouse" ), "northEastWoodRoofHouse" );
	world.setobj[world.setobj.length()-1].setPosition(2210,88);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "rotundaRoof" ), "rotundaRoof" );
	world.setobj[world.setobj.length()-1].setPosition(1007,807);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "sEWall" ), "sEWall" );
	world.setobj[world.setobj.length()-1].setPosition(1846,2818);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "sEWoodThatchRoof" ), "sEWoodThatchRoof" );
	world.setobj[world.setobj.length()-1].setPosition(2410,2178);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "southTreeCluster" ), "southTreeCluster" );
	world.setobj[world.setobj.length()-1].setPosition(1004,358);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "strawRoof" ), "strawRoof" );
	world.setobj[world.setobj.length()-1].setPosition(1112,1986);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "strawRoofEastGray" ), "strawRoofEastGray" );
	world.setobj[world.setobj.length()-1].setPosition(1564,2304);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "strawRoofEastGrayCross" ), "strawRoofEastGrayCross" );
	world.setobj[world.setobj.length()-1].setPosition(1564,2018);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "sWThatchHouse" ), "sWThatchHouse" );
	world.setobj[world.setobj.length()-1].setPosition(1,1942);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "sWWall" ), "sWWall" );
	world.setobj[world.setobj.length()-1].setPosition(358,3030);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "sWWall2" ), "sWWall2" );
	world.setobj[world.setobj.length()-1].setPosition(874,2572);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "thatchRoof" ), "thatchRoof" );
	world.setobj[world.setobj.length()-1].setPosition(1564,590);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "topTree" ), "topTree" );
	world.setobj[world.setobj.length()-1].setPosition(1428,76);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "topTreeBottom" ), "topTreeBottom" );
	world.setobj[world.setobj.length()-1].setPosition(1372,72);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "treetops" ), "treetops" );
	world.setobj[world.setobj.length()-1].setPosition(796,1570);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "wall" ), "wall" );
	world.setobj[world.setobj.length()-1].setPosition(1,1);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "westTree" ), "westTree" );
	world.setobj[world.setobj.length()-1].setPosition(178,1136);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	world.add( world.getStaticEntity( "VillageTown", "westTreeCluster" ), "westTreeCluster" );
	world.setobj[world.setobj.length()-1].setPosition(24,1936);
	world.setobj[world.setobj.length()-1].setCollidable(true);
	// TODO: Set up world
	finishWorldSetup( world.getName() );
}

void setUpPriamAltar() {
	ee::consolePrintln( "setUpPriamAltar in worldloader.as");
	world.add( world.getStaticEntity( "PriamAltar", "walls"), "walls" );

	finishWorldSetup( world.getName() );
}

void setUpCityThree() {
	ee::consolePrintln( "setUpCityThree in worldloader.as");
	world.add( world.getStaticEntity( "TrojanCity1", "leftColumn" ), "leftColumn" );
	world.add( world.getStaticEntity( "TrojanCity1", "leftWall" ), "leftWall" );
	world.add( world.getStaticEntity( "TrojanCity1", "overlapTrees" ), "overlapTrees" );
	world.add( world.getStaticEntity( "TrojanCity1", "rightColumn" ), "rightColumn" );
	world.add( world.getStaticEntity( "TrojanCity1", "southernBuilding" ), "southernBuilding" );
	world.add( world.getStaticEntity( "TrojanCity1", "Trees" ), "Trees" );

	finishWorldSetup( world.getName() );

}

void setUpCityTwo() {
	ee::consolePrintln( "setUpCityTwo in worldloader.as");
	world.add( world.getStaticEntity( "TrojanCity2", "leftColumn" ), "leftColumn" );
	world.add( world.getStaticEntity( "TrojanCity1", "rightColumn" ), "rightColumn" );

	finishWorldSetup( world.getName() );
}

void setUpCityOne() {
	ee::consolePrintln( "setUpCityOne in worldloader.as");
	world.add( world.getStaticEntity( "TrojanCity1", "leftColumn" ), "leftColumn" );
	world.add( world.getStaticEntity( "TrojanCity1", "palaceOverlap" ), "palaceOverlap" );
	world.add( world.getStaticEntity( "TrojanCity1", "rightColumn" ), "rightColumn" );
	world.add( world.getStaticEntity( "TrojanCity1", "rightWall" ), "rightWall" );

	finishWorldSetup( world.getName() );
}

void destroyOldWorld( int lvl ) {
	ee::consolePrintln( "destroyOldWorld in worldloader.as");
	world.destroyOldWorld( lvl );
}

void setUpBoundary() {
	ee::consolePrintln( "setupboundary in worldloader.as");
	world.add(world.getStaticEntity(""))
}