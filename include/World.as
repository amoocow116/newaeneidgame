// Name: World.as
// Author(s):
// version 0.1
// --------------------------------------
// These classes are used to define the 
// world the other assets will be in

import bool saveAIChars() from "TrojanGreek.as";
import bool saveCitizens() from "Citizen.as"; //prob not gonna use this 
import bool requestSaveData() from "Aeneas.as";
import CharPosition @ getAeneasPos() from "Aeneas.as";

#include "Movable.as"

class World
{
	array< ee::StaticEntity > setobj; // Expecting method, instead receives identifier
	array< ee::AnimatedEntity > movingobj; // Expecting method, instead receives identifier
	private int layer;
	Checkpoint checkpoint;
	Checkpoint exitCheckpoint;
	string name;
	int EXIT_RANGE = 2500;

	World( string name )
	{
		ee::consolePrintln( "constructor in world.as");
		bool checkPointUsed = false;
		ee::readFromDataCont( name, "checkPointUsed", checkPointUsed );
		ee::consolePrintln( "Constructor for World. Sets the base layer for World." );
		layer = 1000;
		setName( name );

		setupCheckpoint( 0, 0 ); // Temporary placeholder until setupCheckpoint is called again to make actual checkpoint
		checkpoint.setCheckPointUsed( checkPointUsed );
	}

	bool isCompleted() {
		ee::consolePrintln( "isCompleted() in world.as");
		CharPosition aeneaspos = getAeneasPos();
		float xdif = aeneaspos.getX() - exitCheckpoint.getX();
		float ydif = aeneaspos.getY() - exitCheckpoint.getY();
		float difference = sqrt( pow( xdif, 2 ) + pow( ydif, 2 ) );
		float radius = EXIT_RANGE; // arbitrary value
		if( difference < radius ) {
			return true;
		}
		return false;
	}

	void destroyOldWorld( int lvl ) {
		ee::consolePrintln( "destroying old world in world.as");
		for( int i = 0; i < setobj.length(); i++ ) {
			setobj[ setobj.length() - 1 ].remove();
			setobj.removeLast();
		}
		for( int i = 0; i < movingobj.length(); i++ ) {
			movingobj[ movingobj.length() - 1 ].remove();
			movingobj.removeLast();
		}
	}

	void setupCheckpoint( int x, int y ) {
		// DO NOT SET COLLIDEABLE
		ee::consolePrintln( "setting up a checkpoint in world.as");
		ee::StaticEntity e = StaticEntity( "HUD", "health" ); // setCollideable( true );
		e.setCollideable( false );
		e.setVisible( false );
		checkpoint = Checkpoint( x, y, e );
	}

	void setupExitPoint( int x, int y ) {
		// DO NOT SET COLLIDEABLE
		ee::consolePrintln( "setting up exitpoint in world.as");
		ee::StaticEntity e = StaticEntity( "HUD", "health" ); // setCollideable( true );
		setCollideable( false );
		e.setVisible( false );
		exitCheckpoint = Checkpoint( x, y, e, true );
	}

	void setCheckPointWasUsed( bool b ) {
		ee::consolePrintln( "setcheckpointwasused in world.as");
		checkpoint.setCheckPointUsed( b );
	}

	void step( uint32 milliseconds ) {
		
		checkpoint.step( name, milliseconds );
	}

	// Creates arbitrary Checkpoint to use the saveData function
	void saveData() { Checkpoint().saveData(); }

	void add( ee::AnimatedEntity x, string name ) 
	{
		ee::consolePrintln( "Adds animated entity to movingobj array. Adds object to render at layer input relative to the World Layer." );
		movingobj.insertLast( x );
	}

	void add( ee::StaticEntity x, string name )
	{
		ee::consolePrintln( "Adds static entity to movingobj array. Adds object to render at layer input relative to the World Layer." );
		setobj.insertLast( x );
	}

	void setName( string n ) {
		name = n;
	}

	string getName() {
		return name;
	}

	ee::StaticEntity getStaticEntity( string contName, string entName ) {
		ee::consolePrintln( "returning static entity in world.as");
		return ee::StaticEntity( contName, entName );
	}

	ee::AnimatedEntity getAnimatedEntity( string contName, string entName ) {
		ee::consolePrintln( "returning animated entity in world.as");
		return ee::AnimatedEntity( contName, entName );
	}
}

//checkpoint saves the character, and stuff, when the character runs over the checkpoint
// Assumes only one checkpoint in each World (at beginning of map)
class Checkpoint
{
	private ee::StaticEntity checkp;
	private CharPosition checkpoint;
	bool checkPointUsed;
	bool isExitPoint;
	int xPos;
	int yPos;

	Checkpoint() {
		checkPointUsed = false;
		isExitPoint = false;
	}
	
	Checkpoint( int x, int y, ee::StaticEntity check )
	{
		ee::consolePrintln( "Creates position for the Checkpoint." );
		checkp = check;

		checkPointUsed = false;
		isExitPoint = false;

		xPos = x;
		yPos = y;
	}

	Checkpoint( int x, int y, ee::StaticEntity check, bool b )
	{
		checkp = check;
		isExitPoint = b;

		xPos = x;
		yPos = y;
	}

	void setCheckPointUsed( bool cpu ) {
		checkPointUsed = cpu;
	}

	int getX() { return xPos; }
	int getY() { return yPos; }
	
	void step(string name, uint32 milliseconds)
	{
		if( isExitPoint )
			{
			ee::consolePrintln( "if in step function of checkpoint class, there is an exit point (isExitPoint == true");
			return;
		CharPosition aeneaspos = getAeneasPos();
		float xdif = abs( aeneaspos.getX() - checkpoint.getX() );
		float ydif = abs( aeneaspos.getY() - checkpoint.getY() );
		float difference = sqrt( pow( xdif, 2 ) + pow( ydif, 2 ) );
		float radius = 2500;
		if( difference < radius && !checkPointUsed ) {
			ee::consolePrintln( "checkpoint data is about to be saved in world.as after this console print line");
			saveData();
			checkPointUsed = true;

			// Allows future saves to see that the checkpoint in world "name" has been used
			ee::writeToDataCont( name, "checkPointUsed", true );
		}
	}

	void saveData() {
		ee::consolePrintln( "requesting save data in checkpoint");
		requestSaveData();
		saveAIChars();
	}
}



