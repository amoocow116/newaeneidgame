//#include "Menu.as"
//#include "Button.as"

class MainMenu// : Menu
{
	bool buttonPressed = false;
	ee::AnimatedEntity background;
	Text text;
	uint32 timeInMillis;
	bool textIsVisible;
	
	MainMenu() {
		//Menu();
		background = ee::AnimatedEntity( "MainMenu", "background" );
		background.setVisible( true );
		text = Text();
		text.setString( "Press Space to continue" );
		text.makeVisible( false );
		textIsVisible = false;

		timeInMillis = 0;
	}
	
	void step( uint32 milliseconds ) {

		if( timeInMillis >= 12000 && !textIsVisible ) {
			text.makeVisible( true );
		} else {
			timeInMillis += milliseconds;
		}
		
		if( ee::isKeyPressed( Space ) && timeInMillis >= 12000 )  //any key or any button
		{
			buttonPressed = true;
			//background.clearAllEntityToRender();
			background.setVisible(false);
			background.remove();
		}
	}
	
	bool buttonWasPressed()
	{
		return buttonPressed;
	}
}