// Name: CutsceneManager.as
// Author(s): Jason Wang
// version 0.1
// --------------------------------------
// This module sets up stuff for cutscenes

#include "include/AIChar.as"
#include "include/CharStats.as"

/*
Notes:
Simply use a text object
Same position functions as an entity
setCharacterSize( int ) is the only difference
setString() to set text
*/

Text text;
protected ee::StaticEntity characterImage;
// entityAttackMove = ee::AnimatedEntity( "Character", string name );

void initialize() {
    text = Text();
}

void step( uint32 milliseconds ) {
	
}

void startCutscene( string text, int length, string name ) {

    characterImage = ee::StaticEntity( "Character", name );
	text.setCharacterSize(length);
	text.setString(text);
    characterImage.setVisible( true );
    characterImage.setCollidable( false );
	//some sort of waiting function, then probably do a return; to go back to whatever was happening in the game??? 
}


