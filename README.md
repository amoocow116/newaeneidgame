# README #

This is the README for the Aeneid Game, which is to be demonstrated at the California JCL State Convention 2016.

## Formatting ##

* class and interface names must start out as capatalized
* variable names and methods must start lower case
* curly brace position, most prefer on own line it seems
* module name must be of main class in file
* non executable things (things that will be included) do not need initialize or step functions
* add the little info at the beginning of each of the module so people know what it is 
* all modules intended to be included go into the include folder, else they go into the main one (for executable modules)